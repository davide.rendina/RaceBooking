## EXPOSED ENDPOINT
This microservices listen port 8000 and communicates in JSON format. The available endpoints are:
- **GET /id/booking** GET new id for a booking resource
- **GET /id/racetrack** GET new id for a racetrack resource


## ID Structure

| Item (2) | Date (6) | Time (9) | Control code (1) |

Where:
- Item is 2 char representing the type of resource (ex. BK for booking IDs)
- Date is the date in format YYMMDD
- Time is the time in format HHMMSSmmm (where m is milliseconds)
- Control code is a character used for preventing the creation of the same ID

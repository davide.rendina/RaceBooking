## EXPOSED ENDPOINT
This microservices listen port 8000 and communicates in JSON format. The available endpoints are described in the main README.md of the system.

#### JSON example

###### BOOKING

    {
        "name": "Davide Rendina",
        "email": "daviderendina9@gmail.com",
        "bookingDate": "2022-10-01",
        "entranceBooked": "3",
        "raceTrack": {
            "id": "<trackID>"
        }
    }

###### TRACK

    {
        "name": "Misano World Circuit Marco Simoncelli",
        "address": "Via Adriatica 58",
        "lengthInMeters": "2500",
        "maxDailyBooking": "100"
    }


____
## REQUIRED ENVIRONMENT VARIABLES

- **BOOKING_ENDPOINT** URI of Booking microservice
- **RACETRACK_ENDPOINT** URI of Booking microservice

package com.racebooking.server;

import com.racebooking.model.Booking;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.racebooking.model.RaceTrack;
import com.racebooking.services.BookingService;
import com.racebooking.services.database.DatabaseHandler;
import com.racebooking.services.IDGeneratorService;
import com.racebooking.services.logging.Logger;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.stream.Collectors;

public class RequestHandler {

    private static DatabaseHandler dbHandler;

    public static void init(){
        dbHandler = new DatabaseHandler();
    }


    public static Object getAllHandler(Request request, Response response) {
        Logger.log("Received GET ALL request");

        List<RaceTrack> trackList = dbHandler.getAll();
        if(trackList == null){
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot retrieve the complete RaceTrack list from the database");
        }

        return generateResponseJSON(HttpStatus.OK_200, trackList);
    }

    public static Object getHandler(Request request, Response response) {
        String id = request.params(":id");
        Logger.log("Received GET request of resource " + id);

        RaceTrack track = dbHandler.get(id);
        if(track == null){
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot perform SQL operations. Check SQL syntax or the status of the database.");
        }
        if(track.getId() == null) {
            response.status(HttpStatus.NOT_FOUND_404);
            return generateErrorResponseJSON(HttpStatus.NOT_FOUND_404, "Cannot found a RaceTrack with the provided id: " + id);
        }

        return generateResponseJSON(HttpStatus.OK_200, track);
    }

    public static Object createHandler(Request request, Response response) {
        Logger.log("Received POST request with body: "+request.body());

        RaceTrack raceTrack = buildRaceTrackFromRequest(request.body());
        if(raceTrack == null) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot build resource from provided JSON data: "+request.body());
        }

        if(stringEmptyOrNull(raceTrack.getName()) || stringEmptyOrNull(raceTrack.getAddress())) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Field values cannot be null, missing or less than zero");
        }

        if(raceTrack.getMaxDailyBooking() <= 0 || raceTrack.getLengthInMeters() <= 0) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Field values cannot be null, missing or less than zero");
        }


        String racetrackID = IDGeneratorService.generateID();
        if(stringEmptyOrNull(racetrackID)) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot find IDGeneration microservice or got an invalid ID");
        }

        raceTrack.setId(racetrackID);

        boolean result = dbHandler.create(raceTrack);

        response.status(result ? 200 : 500);

        return result ?
                generateResponseJSON(HttpStatus.OK_200, raceTrack) :
                generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Error while creating the raceTrack");
    }

    public static Object updateHandler(Request request, Response response) {
        String id = request.params(":id");
        Logger.log("Received PUT request on resource " + id);

        RaceTrack updatedTrack = buildRaceTrackFromRequest(request.body());
        if(updatedTrack == null) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot build resource starting from JSON received in body request: " + request.body());
        }
        updatedTrack.setId(id);

        RaceTrack actualTrack = dbHandler.get(id);
        if(actualTrack == null) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot perform SQL operations. Check SQL syntax or the status of the database.");
        }
        if(actualTrack.getId() == null) {
            response.status(HttpStatus.NOT_FOUND_404);
            return generateErrorResponseJSON(HttpStatus.NOT_FOUND_404, "Cannot found a RaceTrack with the provided id: " + id);
        }

        if(!validateRaceTrackValues(updatedTrack)) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Provided data are not correct: check that they are not null, empty or less-equal than 0");
        }

        List<Booking> bookingList = BookingService.getAllBookingByTrackId(id);
        if(bookingList == null) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Error while retrieving booking list");
        }
        boolean cantReduceBookings = bookingList.stream().collect(Collectors.groupingBy(Booking::getBookingDate))
                .entrySet().stream()
                .anyMatch(map -> {
                    int sum = 0;
                    for(Booking b : map.getValue())
                        sum += b.getEntranceBooked();
                    return sum > updatedTrack.getMaxDailyBooking();
                });
        if(cantReduceBookings){
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot reduce maxDailyBooking: in some days, booked places are greater than the selected value");
        }

        boolean result = dbHandler.update(updatedTrack);

        response.status(result ? 200 : 500);

        return result ?
                generateResponseJSON(HttpStatus.OK_200, updatedTrack) :
                generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot update track values (trackID "+updatedTrack.getId()+"). This may due to an SQLException, wrong provided id or JSON parsing Exception");
    }

    public static Object deleteHandler(Request request, Response response) {
        String id = request.params(":id");
        Logger.log("Received DELETE request of resource " + id);

        RaceTrack raceTrack = dbHandler.get(id);
        if(raceTrack == null){
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot perform SQL operations. Check SQL syntax or the status of the database.");
        }
        if(raceTrack.getId() == null){
            response.status(HttpStatus.NOT_FOUND_404);
            return generateErrorResponseJSON(HttpStatus.NOT_FOUND_404, "Cannot found a RaceTrack with the provided id: "+id);
        }

        List<Booking> bookingList = BookingService.getAllBookingByTrackId(id);
        if(bookingList == null){
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Error while retrieving booking list");
        }
        if(! bookingList.isEmpty()) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot delete track with booking associated");
        }

        boolean result = dbHandler.delete(id);
        response.status(result ? 200 : 500);
        return result ?
                generateErrorResponseJSON(HttpStatus.OK_200, "Resource "+id+" deleted correctly") :
                generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot execute SQL command (raised SQLException)");
    }


    private static RaceTrack buildRaceTrackFromRequest(String requestBody){
        RaceTrack track = null;
        try {
            if(requestBody.equals("{}"))
                return null;
            else
                track = new ObjectMapper()
                    .registerModule(new JavaTimeModule())
                    .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                    .readValue(requestBody.replace('\'', '\"'), RaceTrack.class);
        } catch (JsonProcessingException ex){
            Logger.log("Raised exception while parsing RaceTrack: "+ex.getMessage());
            return null;
        }
        return track;
    }

    private static boolean stringEmptyOrNull(String st){
        return st == null || st.equals("");
    }

    private static boolean validateRaceTrackValues(RaceTrack raceTrack) {
        return ! stringEmptyOrNull(raceTrack.getName()) &&
                ! stringEmptyOrNull(raceTrack.getAddress()) &&
                ! (raceTrack.getMaxDailyBooking() <= 0) &&
                ! (raceTrack.getLengthInMeters() <= 0);
    }

    private static String generateErrorResponseJSON(int code, String msg){
        ObjectNode resp = new ObjectMapper().createObjectNode();
        resp.put("status", code);
        resp.put("data",msg);
        return resp.toString();
    }

    private static String generateResponseJSON(int code, List<RaceTrack> trackList){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resp = mapper.createObjectNode();
        resp.put("status", code);
        ArrayNode array = resp.putArray("data");

        for(RaceTrack rt : trackList){
            ObjectNode track = mapper.createObjectNode();
            track.put("id",rt.getId());
            track.put("name",rt.getName());
            track.put("address",rt.getAddress());
            track.put("lengthInMeters",rt.getLengthInMeters());
            track.put("maxDailyBooking",rt.getMaxDailyBooking());

            array.add(track);
        }
        return resp.toString();
    }

    private static String generateResponseJSON(int code, RaceTrack raceTrack){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resp = mapper.createObjectNode();
        resp.put("status", code);
        ObjectNode track = resp.putObject("data");

        track.put("id", raceTrack.getId());
        track.put("name", raceTrack.getName());
        track.put("address", raceTrack.getAddress());
        track.put("lengthInMeters", raceTrack.getLengthInMeters());
        track.put("maxDailyBooking", raceTrack.getMaxDailyBooking());

        return resp.toString();
    }

}

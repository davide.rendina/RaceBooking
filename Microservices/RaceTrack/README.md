## EXPOSED ENDPOINT
This microservices listen port 8000 and communicates in JSON format. The available endpoints are:
- **GET /racetrack** Retrieve all racetrack
- **GET /racetrack/{id}** Retrieve single racetrack by id
- **POST /racetrack** Create the resource passing the specified racetrack as JSON in the body of the request.
- **PUT /racetrack/{id}** Update the resource specified by passing a JSON in the body with new values.
- **DELETE /racetrack/{id}** Delete specified racetrack

###### JSON example

    {
        "name": "Misano World Circuit Marco Simoncelli",
        "address": "Via Adriatica 58",
        "lengthInMeters": "2500",
        "maxDailyBooking": "100"
    }

____
## REQUIRED ENVIRONMENT VARIABLES

- **DATABASE_HOSTNAME**: the hostname of the database
- **DATABASE_NAME**: the name of the database
- **DATABASE_PASSWORD**: password for accessing the database
- **DATABASE_USER**: username for accessing the database


- **BOOKING_RACETRACK_ENDPOINT**: hostname+endpoint for accessing booking filtered by racetrack (ex. /booking/racetrack)
- **ID_GENERATOR_API**: hostname and URI for retrieve ID for new resources

import mysql.connector as mc
import os
from datetime import datetime
import time

def log(text):
    print(datetime.now().strftime("[%d/%m/%Y %H:%M:%S:%m]"),text)


MYSQL_ROOT = os.environ['MYSQL_ROOT']
MYSQL_ROOT_PASSWORD = os.environ['MYSQL_ROOT_PASSWORD']

MYSQL_HOST = os.environ['MYSQL_HOST']
MYSQL_DATABASE = os.environ['MYSQL_DATABASE']
TABLE_RACETRACK = "RaceTrack"

MYSQL_USER = os.environ['MYSQL_USER']
MYSQL_USER_PASSWORD = os.environ['MYSQL_USER_PASSWORD']

CONNECTION_WAIT = 15


log("Starting initialization..")
log("Host: "+MYSQL_HOST)
log("Database: "+MYSQL_DATABASE)
log("Table: "+TABLE_RACETRACK)


attempts = 10
is_connected = False
while attempts > 0 and not is_connected:
    attempts -= 1
    try:
        log("Trying connection. Remaining attempts: "+str(attempts))
        dbconn = mc.connect(user=MYSQL_ROOT, password=MYSQL_ROOT_PASSWORD, host=MYSQL_HOST)
        is_connected = True
    except mc.Error as e:
        log("Exception thrown: "+format(e))
        log("Trying to access with: "+MYSQL_ROOT+"@"+MYSQL_HOST)
        log("Program will sleep for {}sec. Remaining attempts: {}".format(CONNECTION_WAIT, str(attempts)))
        time.sleep(CONNECTION_WAIT)
if not is_connected:
    raise Exception("Cannot access database. See logs for more info")


if dbconn.is_connected():
    # Check if the database exists, otherwise create a new one
    dbCursor = dbconn.cursor()
    dbCursor.execute("SHOW DATABASES")
    dbs = [ i[0] for i in dbCursor.fetchall() ]
    if MYSQL_DATABASE not in dbs:
        dbCursor.execute("CREATE DATABASE {database}".format(database=MYSQL_DATABASE))
        log("Created database:" + MYSQL_DATABASE)
    else:
        log("Database {database} also exists".format(database=MYSQL_DATABASE))
    dbCursor.close()
    dbconn.database = MYSQL_DATABASE

    # Check user present
    userCursor = dbconn.cursor()
    userCursor.execute("select user from mysql.user");
    users = [ i[0] for i in userCursor.fetchall() ]
    if MYSQL_USER not in users:
        userCursor.execute("CREATE USER '{user}'@'{host}' IDENTIFIED BY '{password}';"
            .format(user=MYSQL_USER, host="localhost", password=MYSQL_USER_PASSWORD))
        log("Created user: "+MYSQL_USER)
    else:
        log("User {user} also exists".format(user=MYSQL_USER))
    userCursor.close()

    # Check tables
    tableCursor = dbconn.cursor()
    tableCursor.execute("show tables")
    tbs = [ i[0] for i in tableCursor.fetchall() ]
    if TABLE_RACETRACK not in tbs:
        tableCursor.execute("CREATE TABLE {table} (id varchar(21) primary key, name varchar(60), address varchar(70), lengthInMeters integer, maxDailyBooking integer)"
            .format(table=TABLE_RACETRACK))
        log("Created table: "+TABLE_RACETRACK)
    else:
        log("Table {table} also exists".format(table=TABLE_RACETRACK))
    tableCursor.close()

    # Close connection
    dbconn.close()
else:
    log("Impossible to connect to the database")

package com.racebooking.services;

import com.racebooking.model.JSONResponse;
import com.racebooking.model.JSONResponseFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class IDGeneratorService {

    final static String ID_GENERATOR_ENDPOINT = "http://" + System.getenv("ID_GENERATOR_ENDPOINT") + "/booking";

    public static String generateID(){
        URL url;
        HttpURLConnection httpURLConnection;
        int responseCode;

        try {
            url = new URL(ID_GENERATOR_ENDPOINT);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            responseCode = httpURLConnection.getResponseCode();
        } catch (MalformedURLException e) {
            Logger.log("Cannot generate URL (generateID): "+ ID_GENERATOR_ENDPOINT);
            Logger.log("Raised MalformedURLException (generateID): "+e.getMessage());
            return null;
        } catch (ProtocolException e) {
            Logger.log("Raised ProtocolException (generateID): "+e.getMessage());
            return null;
        } catch (IOException e) {
            Logger.log("Raised IOException (generateID): "+e.getMessage());
            return null;
        }

        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in;
            StringBuilder response;
            try {
                in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String inputLine;
                response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                Logger.log("Raised IOException during building response (generateID): "+e.getMessage());
                return null;
            }

            JSONResponse jsonResponse = JSONResponseFactory.fromString(response.toString().replace("\'", "\""));
            if(jsonResponse == null)
                return null;

            Logger.log("ID retrieved successfully (generateID): "+jsonResponse.getData());
            return (String)jsonResponse.getData();
        } else {
            Logger.log("GET request not worked (generateID)");
            return null;
        }
    }
}

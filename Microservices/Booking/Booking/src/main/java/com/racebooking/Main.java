package com.racebooking;

import com.racebooking.services.Logger;
import com.racebooking.server.RequestHandler;

import static spark.Spark.*;


public class Main {

    static int listeningPort = 8000;

    public static void main(String[] args) {

        Logger.log("Listening server on localhost:" + listeningPort);
        port(listeningPort);
        RequestHandler.init();

        // Get all resources
        get("/booking", RequestHandler::getAllHandler);

        // Get single resource
        get("/booking/:id", RequestHandler::getHandler);

        // Create resource
        post("/booking", RequestHandler::createHandler);

        // Update resource
        put("/booking/:id", RequestHandler::updateHandler);

        // Delete resource
        delete("/booking/:id", RequestHandler::deleteHandler);

        // Get all booking of a track
        get("/booking/racetrack/:id", RequestHandler::getAllByTrackHandler);
    }
}


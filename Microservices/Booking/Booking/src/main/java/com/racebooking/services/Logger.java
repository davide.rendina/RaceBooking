package com.racebooking.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {
    public static void log (String message){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss:SSS");
        System.out.println("[" + dtf.format(LocalDateTime.now()) + "] " + message);
    }
}

package com.racebooking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONResponse {
    @JsonProperty("status")
    String status;
    @JsonProperty("data")
    Object data;

    public JSONResponse(){}

    public Object getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }
}

package com.racebooking.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.racebooking.services.RaceTrackService;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RaceTrack {
    String id;
    int maxDailyBooking = -1;
    String name;
    String address;

    public RaceTrack(){}

    public RaceTrack(String id) {
        this.id = id;
    }

    public int getMaxDailyBooking() {
        if(maxDailyBooking == -1){
            RaceTrack raceTrack = RaceTrackService.getRaceTrack(this.id);
            maxDailyBooking = raceTrack == null ? -1 : raceTrack.getMaxDailyBooking();
        }
        return this.maxDailyBooking;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
}

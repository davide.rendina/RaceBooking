package com.racebooking.services;

import com.racebooking.model.Booking;
import com.racebooking.model.RaceTrack;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler {

    static String databaseHostname = System.getenv("DATABASE_HOSTNAME");
    static String databaseName = System.getenv("DATABASE_NAME");
    static String databasePassword = System.getenv("DATABASE_PASSWORD");
    static String databaseUser = System.getenv("DATABASE_USER");

    Connection dbconn;
    boolean connectionStatus = false;

    public DatabaseHandler() {
        connectToDB();
    }

    public void connectToDB() {
        int remainingAttempt = 10;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex){
            Logger.log("Exception raised while searching for jdbc class driver when connecting to DB");
            System.out.println(ex.getMessage());
        }

        String url = "jdbc:mysql://" + databaseHostname + "/" + databaseName;
        Logger.log("Trying to connect with the url: "+url);

        while(dbconn == null){
            try {
                if(remainingAttempt > 0){
                    dbconn = DriverManager.getConnection(url, databaseUser, databasePassword);
                } else {
                    Logger.log("DB Connection status: cannot connect to the database");
                    break;
                }

            } catch (SQLException ex) {
                Logger.log("Cannot connect to DB: "+ex.getMessage());
                Logger.log(" Remaining attempts: " + remainingAttempt);
                try {
                    Thread.sleep(15000);
                    remainingAttempt--;
                } catch (InterruptedException e) {Logger.log("Raised InterruptedException while thread sleep. Remaining attempts: " + remainingAttempt);}
            }
        }

        if(remainingAttempt > 0){
            connectionStatus = true;
            Logger.log("DB Connection status: OK");
        }
    }

    public Booking get(String id){
        Booking booking = new Booking();

        try {
            String query = "select * from Booking where id = ?";
            PreparedStatement preparedStatement = dbconn.prepareStatement(query);
            preparedStatement.setString(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) {
                booking = new Booking(
                    rs.getString("id"),
                    rs.getString("name"),
                    rs.getDate("bookingDate").toLocalDate(),
                    rs.getTimestamp("timestamp"),
                    rs.getInt("entranceBooked"),
                    new RaceTrack(rs.getString("trackId")),
                    rs.getString("email")
                );
            }
            Logger.log(String.format("Retrieved booking %s from database.", id));

        } catch (SQLException ex) {
            Logger.log("SQLException raised: " + ex.getMessage());
            return null;
        }

        return booking;
    }

    public List<Booking> getAll(){
        String query = "Select * from Booking";
        List<Booking> bookingList = new ArrayList<>();
        try {
            ResultSet rs = dbconn.createStatement().executeQuery(query);
            while(rs.next()){
                Booking booking = new Booking(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getDate("bookingDate").toLocalDate(),
                        rs.getTimestamp("timestamp"),
                        rs.getInt("entranceBooked"),
                        new RaceTrack(rs.getString("trackID")),
                        rs.getString("email")
                );
                bookingList.add(booking);
            }
        } catch (SQLException ex){
            Logger.log("SQLException raised: "+ex.getMessage());
            return null;
        }

        Logger.log("Retrieved result from database. Total booking retrieved: " + bookingList.size());
        return bookingList;
    }

    public boolean delete(Booking booking){
        if(booking == null){
            Logger.log("Resource not found: ID " + booking.getId());
            return false;
        } else if (booking.getBookingDate().isBefore(LocalDate.now())) {
            Logger.log("Operation aborted: trying to delete past booking");
            return false;
        } else {
            try {
                PreparedStatement preparedStatement = dbconn.prepareStatement("DELETE from Booking where id = ?");
                preparedStatement.setString(1, booking.getId());
                preparedStatement.execute();
                boolean result = preparedStatement.getUpdateCount() > 0;

                Logger.log("Process finished with result: " + result);
                return result;
            } catch (Exception ex){
                Logger.log("Cannot delete resource " + booking.getId());
                return false;
            }
        }
    }

    public boolean create(Booking booking){
        String insert = "INSERT INTO Booking (id, bookingDate, name, timestamp, entranceBooked, trackID, email) VALUES (?, ?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement preparedStatement = dbconn.prepareStatement(insert);
            preparedStatement.setString(1, booking.getId());
            preparedStatement.setString(2, booking.getBookingDate().toString());
            preparedStatement.setString(3, booking.getName());
            preparedStatement.setString(4, booking.getTimestamp().toString());
            preparedStatement.setString(5, String.valueOf(booking.getEntranceBooked()));
            preparedStatement.setString(6, booking.getRaceTrack().getId());
            preparedStatement.setString(7, booking.getEmail());

            int result = preparedStatement.executeUpdate();
            Logger.log(result != -1 ? "Booking inserted correctly with id " + booking.getId() : "The booking has not been inserted");

            return result != -1;

        } catch (SQLException e) {
            Logger.log("Raised SQLException while inserting new booking: "+e.getMessage());
            return false;
        }
    }

    public boolean update(Booking updatedBooking){
        try{
            Logger.log("Trying to update booking with id " + updatedBooking.getId());
            PreparedStatement preparedStatement = dbconn.prepareStatement("UPDATE Booking set name=?,bookingDate=?,trackId=?,email=?,entranceBooked=? where id = ?");
            preparedStatement.setString(1, updatedBooking.getName());
            preparedStatement.setString(2, updatedBooking.getBookingDate().toString());
            preparedStatement.setString(3, updatedBooking.getRaceTrack().getId());
            preparedStatement.setString(4, updatedBooking.getEmail());
            preparedStatement.setString(5, String.valueOf(updatedBooking.getEntranceBooked()));
            preparedStatement.setString(6, updatedBooking.getId());
            preparedStatement.executeUpdate();

            return preparedStatement.getUpdateCount() > 0;
        } catch (SQLException ex){
            Logger.log("SQLException raised: "+ex.getMessage());
            return false;
        }
    }

    public int getBookingCountOn(RaceTrack raceTrack, LocalDate bookingDate) {
        String query = "Select sum(entranceBooked) as count from Booking where trackId = ? and bookingDate = ?";
        PreparedStatement preparedStatement = null;
        int totalBooking = -1;

        try {
            preparedStatement = dbconn.prepareStatement(query);
            preparedStatement.setString(1, raceTrack.getId());
            preparedStatement.setString(2, bookingDate.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next())
                totalBooking = rs.getInt("count");
        } catch (SQLException e) {
            Logger.log("Raised SQLException while checking availability: "+e.getMessage());
            return -1;
        }
        return totalBooking;
    }

    public List<Booking> getAllByTrack(String trackId) {
        String query = "Select * from Booking where trackID = ?";
        List<Booking> bookingList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = dbconn.prepareStatement(query);
            preparedStatement.setString(1, trackId);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                Booking booking = new Booking(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getDate("bookingDate").toLocalDate(),
                        rs.getTimestamp("timestamp"),
                        rs.getInt("entranceBooked"),
                        new RaceTrack(rs.getString("trackID")),
                        rs.getString("email")
                );
                bookingList.add(booking);
            }
        } catch (SQLException ex){
            Logger.log("SQLException raised: "+ex.getMessage());
            return null;
        }

        Logger.log("Retrieved result from database. Total booking retrieved: " + bookingList.size());
        return bookingList;
    }
}

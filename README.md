# RaceBooking

RaceBooking is a microservice application with the purpose of making reservations for any racetrack and its services.

The application has been developed for the Cloud Computing 2021/22 course of the University of Milan - Bicocca.
____
## System Architecture

The application is composed by 4 microservices and 2 database, and it's ready to deploy in Kubernetes (tested with Minikube only on Debian 10).

In the next _commands_ section are present all the command needed in order to run the application. After following the commands, the application will be available on: <p align="center">```minikube-ip:30080```</p>

The application can be reached from shell using the command:
```sh
 $(minikube ip):30080
```

### Architectural elements

###### Databases

- **booking-database**: MySQL database for Booking
- **racetrack-database**: MySQL database for RaceTrack

###### Microservices
- **Booking** Manage Bookings
- **RaceTrack** Manage Racetrack
- **APIGateway** Expose the API to the external environment. A detailed description of the API are described in the next section _API description_.
- **IDGenerator** Can generate ID for each resource

Each microservice is described in RaceBooking/Microservices subfolders.

### API description
The available endpoints are:
- **GET /booking** Retrieve all booking
- **GET /booking/{id}** Retrieve single booking by id
- **GET /booking/racetrack/{id}** Retrieve all booking of a specified racetrack
- **POST /booking** Create the resource passing the specified booking as JSON in the body of the request.
- **PUT /booking/{id}** Update the resource specified by passing a JSON in the body with new values.
- **DELETE /booking/{id}** Delete specified booking

- **GET /racetrack** Retrieve all racetrack
- **GET /racetrack/{id}** Retrieve single racetrack by id
- **POST /racetrack** Create the resource passing the specified racetrack as JSON in the body of the request.
- **PUT /racetrack/{id}** Update the resource specified by passing a JSON in the body with new values.
- **DELETE /racetrack/{id}** Delete specified racetrack

#### JSON example

###### BOOKING

    {
        "name": "Davide Rendina",
        "email": "daviderendina9@gmail.com",
        "bookingDate": "2022-10-01",
        "entranceBooked": "3",
        "raceTrack": {
            "id": "<trackID>"
        }
    }

###### TRACK

    {
        "name": "Misano World Circuit Marco Simoncelli",
        "address": "Via Adriatica 58",
        "lengthInMeters": "2500",
        "maxDailyBooking": "100"
    }

____

## Commands
Clone this repository and set the correct folder
```sh
cd RaceBooking
```

Start minikube service and inizialize it
```sh
minikube start
kubectl create ns racebooking
kubectl config set-context minikube --namespace=racebooking
```

Build the images from the src present in Microservices folder
```sh
eval $(minikube docker-env)

docker build Microservices/Booking/. -t racebooking/booking:1.0.0
docker build Microservices/RaceTrack/. -t racebooking/racetrack:1.0.0
docker build Microservices/IdGenerator/. -t racebooking/idgenerator:1.0.0
docker build Microservices/APIGateway/. -t racebooking/apigateway:1.0.0
docker build Microservices/RaceTrackInitContainerDB/. -t racebooking/racetrack-init:1.0.0
docker build Microservices/BookingInitContainerDB/. -t racebooking/booking-init:1.0.0
```

Crete K8s objects
```sh
cd Deployment

kubectl apply -f ConfigMap/.
kubectl apply -f Secret/.
kubectl apply -f StatefulSet/.
kubectl apply -f Service/.
kubectl apply -f Pod/.

```

The you can access APIGateway (and then the application) with
```sh
curl $(minikube ip):30080
```

A test file is present in root folder and can be launched with
Various test file are present in Test/ folder and they can be launched using the command (just replace X with the nummber of test)
```sh
python3 testcaseX.py http://$(minikube ip):30080
```

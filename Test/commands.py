#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import json
from datetime import datetime


def printResponse(resp, name):
    print (resp.status_code, name)
    data = json.loads(str(resp.content.decode('utf-8')))
    print ('JSON RESPONSE:  ', data, '\n')


def log(text):
    print (datetime.now().strftime('[%d/%m/%Y %H:%M:%S:%m]'), text)


def GET_allracetrack(api_gateway_url_racetrack):
    resp = requests.get(api_gateway_url_racetrack)
    return json.loads(resp.json())


def CREATE_racetrack(api_gateway_url_racetrack, json_racetrack):
    resp = requests.post(api_gateway_url_racetrack, json=json_racetrack)
    return json.loads(resp.json())


def MODIFY_racetrack(api_gateway_url_racetrack, trackID,
                     json_racetrack):
    resp = requests.put(api_gateway_url_racetrack + '/' + trackID,
                        json=json_racetrack)
    return json.loads(resp.json())


def GET_racetrack(api_gateway_url_racetrack, trackID):
    resp = requests.get(api_gateway_url_racetrack + '/' + trackID)
    return json.loads(resp.json())


def DELETE_racetrack(api_gateway_url_racetrack, trackID):
    resp = requests.delete(api_gateway_url_racetrack + '/' + trackID)
    return json.loads(resp.json())


def CREATE_booking(api_gateway_url_booking, jsondata):
    resp = requests.post(api_gateway_url_booking, json=jsondata)
    return json.loads(resp.json())


def GET_allbooking(api_gateway_url_booking):
    resp = requests.get(api_gateway_url_booking)
    return json.loads(resp.json())


def GET_allbooking_bytrack(api_gateway_url_booking, bookID):
    resp = requests.get(api_gateway_url_booking + '/racetrack/'
                        + bookID)
    return json.loads(resp.json())


def GET_booking(api_gateway_url_booking, bookID):
    resp = requests.get(api_gateway_url_booking + '/' + bookID)
    return json.loads(resp.json())


def DELETE_booking(api_gateway_url_booking, bookID):
    resp = requests.delete(api_gateway_url_booking + '/' + bookID)
    return json.loads(resp.json())


def MODIFY_booking(api_gateway_url_booking, bookID, json_racetrack):
    resp = requests.put(api_gateway_url_booking + '/' + bookID,
                        json=json_racetrack)
    return json.loads(resp.json())

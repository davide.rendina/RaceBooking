import commands as c
import sys
import json

api_gateway_url = sys.argv[1]
api_bk = api_gateway_url + "/booking"
api_rt = api_gateway_url + "/racetrack"


print("--- TESTCASE 2 ---")
print()
print()

rt1 = c.CREATE_racetrack(api_rt, {'name':'MWC Marco Simoncelli', 'address':'Via Adriatica 58', 'lengthInMeters':'2500','maxDailyBooking':'100'});
trackID=rt1["data"]["id"]
trackName = rt1["data"]["name"]
#print("POST racetrack - Created RaceTrack '", rt1["data"]["name"], "'")
#print(json.dumps(rt1["data"], indent=4))
#print()

print("POST booking - User want to create a new booking, but the provided trackID doesn't exists.", end="\n")
data = {'name':'Davide Rendina TEST2','email':'daviderendina9@gmail.com','bookingDate':'2023-10-01','entranceBooked':'3','raceTrack':{'id':'123'}}
bk1 = c.CREATE_booking(api_bk, data)
print("ERROR",bk1["status"],"-", bk1["data"])
print()
input("Press any key to continue..\n\n")

print("GET racetrack - The desidered RT get retrieved from the system")
rt1 = c.GET_racetrack(api_rt, trackID)
print(json.dumps(rt1["data"], indent=4))
print()
input("Press any key to continue..\n\n")

print("POST booking - The user correct the trackID in order to make the booking")
data = {'name':'Davide Rendina TEST2','email':'daviderendina9@gmail.com','bookingDate':'2023-10-01','entranceBooked':'3','raceTrack':{'id':'null'}}
data["raceTrack"]["id"] = trackID
bk1 = c.CREATE_booking(api_bk, data)
bkID = bk1["data"]["id"]
print(json.dumps(bk1["data"], indent=4))
print()
input("Press any key to continue..\n\n")

print(".. some day passes, and the user want to change the booking \n \n")

print("PUT booking - A modify request is made, but with wrong values (books for -1)")
data = {'name':'Davide Rendina TEST2','email':'daviderendina9@gmail.com','bookingDate':'2023-10-01','entranceBooked':'-1','raceTrack':{'id':'123'}}
data["raceTrack"]["id"] = trackID
bk1 = c.MODIFY_booking(api_bk, bkID, data)
print("ERROR",bk1["status"],"-", bk1["data"])
print()
input("Press any key to continue..\n\n")

print("PUT booking - The user recognize the error and correct the request. He book for the whole racetrack (100 places)")
data = {'name':'Giorgio Rendina TEST2','email':'daviderendina9@gmail.com','bookingDate':'2023-10-01','entranceBooked':'100','raceTrack':{'id':'null'}}
data["raceTrack"]["id"] = trackID
bk1 = c.MODIFY_booking(api_bk, bkID, data)
print(json.dumps(bk1["data"], indent=4))
print()
input("Press any key to continue..\n\n")

print(".. another user want to make a booking for same track on same day, but the RT is full")
print()

print("POST booking - User make new booking on a full RT")
data = {'name':'Davide Rendina TEST2','email':'daviderendina9@gmail.com','bookingDate':'2023-10-01','entranceBooked':'1','raceTrack':{'id':'null'}}
data["raceTrack"]["id"] = trackID
bk1 = c.CREATE_booking(api_bk, data)
print("ERROR",bk1["status"],"-", bk1["data"])
print()
input("Press any key to continue..\n\n")


print("POST booking - the user decide to change the day, and the booking is ok now.")
data = {'name':'Davide Rendina TEST2','email':'daviderendina9@gmail.com','bookingDate':'2023-10-02','entranceBooked':'3','raceTrack':{'id':'null'}}
data["raceTrack"]["id"] = trackID
bk1 = c.CREATE_booking(api_bk, data)
bkID = bk1["data"]["id"]
print(json.dumps(bk1["data"], indent=4))
print()


print()
print()
print("Don't forget to check your inbox!")
